package nss_edu.shibata.sample.com.recture2homework;

/**
 * Created by yagi on 2017/02/19.
 */

public class Calculator {
    private float mCalcValue;

    public Calculator() {
        mCalcValue = 0.f;
    }

    public float calcAdd(float num) {
        mCalcValue += num;
        return mCalcValue;
    }

    public float calcSub(float num) {
        mCalcValue -= num;
        return mCalcValue;
    }

    public float calcMulti(float num) {
        mCalcValue *= num;
        return mCalcValue;
    }

    public float calcDivide(float num) {
        if (0 == num) {
            // 0 divide
            return -1;
        }

        mCalcValue /= num;
        return mCalcValue;
    }

    public void clear() {
        mCalcValue = 0;
    }

    public float getCalcValue() {
        return mCalcValue;
    }

    public void setCalcValue(float value) {
        mCalcValue = value;
    }

}
