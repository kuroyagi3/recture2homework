package nss_edu.shibata.sample.com.recture2homework;

import android.util.Log;
import android.widget.TextView;

/**
 * Created by yagi on 2017/02/19.
 */

public class DisplayValueController {
    TextView mTvDisplay;

    public DisplayValueController(TextView tvDisplay) {
        this.mTvDisplay = tvDisplay;
    }

    public void clear() {
        this.mTvDisplay.setText("0");
    }

    public float getDisplayValue() {
        float retValue = Float.parseFloat(this.mTvDisplay.getText().toString());
        Log.d("Test", "getInputValue return " + retValue);
        return retValue;
    }

    public void setInputValue(int value) {
        float currentValue = getDisplayValue();
        currentValue *= 10;
        currentValue += value;
        setDisplayValue(currentValue);
    }

    public void setDisplayValue(float value) {
        Log.d("Test", "setDisplayValue set " + value);
        mTvDisplay.setText(String.valueOf(value));
    }
}
