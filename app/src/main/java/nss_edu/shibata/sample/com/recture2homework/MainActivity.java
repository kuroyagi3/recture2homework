package nss_edu.shibata.sample.com.recture2homework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private Calculator mCalc;
    private DisplayValueController mDispValController;

    private boolean mIsCalcKeyPressed = false;
    private int mPrevKey = -1;

    private final int ID    = 0;
    private final int VALUE = 1;
    private final int[][] VIEW_ID_TO_VALUE = {
                                 {R.id.tvKeyOne,   1 },
                                 {R.id.tvKeyTwo,   2 },
                                 {R.id.tvKeyThree, 3 },
                                 {R.id.tvKeyFour,  4 },
                                 {R.id.tvKeyFive,  5 },
                                 {R.id.tvKeySix,   6 },
                                 {R.id.tvKeySeven, 7 },
                                 {R.id.tvKeyEight, 8 },
                                 {R.id.tvKeyNine,  9 },
                                 {R.id.tvKeyZero,  0 }};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCalc = new Calculator();
        mDispValController = new DisplayValueController((TextView)findViewById(R.id.tvCalcResult));
    }

    public void onClickedCalcButton(View view) {
        mIsCalcKeyPressed = true;
        int pressKey = view.getId();

        if (R.id.tvKeyAc == pressKey) {
            mCalc.clear();
            mDispValController.clear();
            mPrevKey = -1;
            return;
        }

        if (-1 == mPrevKey) {
            mCalc.setCalcValue(mDispValController.getDisplayValue());
        }

        float calcValue;
        switch (mPrevKey) {
            case R.id.tvKeyPlusMinus:
                break;
            case R.id.tvKeyPercent:
                break;
            case R.id.tvKeyDivide:
                calcValue = mCalc.calcDivide(mDispValController.getDisplayValue());
                mDispValController.setDisplayValue(calcValue);
                mPrevKey = -1;
                break;
            case R.id.tvKeyMulti:
                calcValue = mCalc.calcMulti(mDispValController.getDisplayValue());
                mDispValController.setDisplayValue(calcValue);
                mPrevKey = -1;
                break;
            case R.id.tvKeySub:
                calcValue = mCalc.calcSub(mDispValController.getDisplayValue());
                mDispValController.setDisplayValue(calcValue);
                mPrevKey = -1;
                break;
            case R.id.tvKeyAdd:
                calcValue = mCalc.calcAdd(mDispValController.getDisplayValue());
                mDispValController.setDisplayValue(calcValue);
                mPrevKey = -1;
                break;
            case R.id.tvKeyEqual:
                calcValue = mCalc.getCalcValue();
                mDispValController.setDisplayValue(calcValue);
                mPrevKey = -1;
                break;
            case R.id.tvKeyPeriod:
                break;
            default:
                break;
        }

        mPrevKey = pressKey;
    }

    public void onClickedNumberButton(View view) {
        if (mIsCalcKeyPressed) {
            mDispValController.clear();
            mIsCalcKeyPressed = false;
        }

        int value = -1;
        for( int i = 0; i < VIEW_ID_TO_VALUE.length; i++) {
            if (VIEW_ID_TO_VALUE[i][ID] == view.getId()) {
                value = VIEW_ID_TO_VALUE[i][VALUE];
            }
        }

        if (-1 != value) {
            mDispValController.setInputValue(value);
        }
    }
}
